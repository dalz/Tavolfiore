![Tavolfiore](tavolfiore.png)

Tavolfiore is a **spartan board game night planner**. It lets users:

- add games they will bring to a public list;

- reserve a spot for games they want to play.

## Setup
To start the server, run `tavolfiore.rkt`. You will need Racket installed.

By default, the server listens on port 58996; you can change it with the
`--port` option.

If you wish to host the server under some path, use the `--root` option.
Sample configuration for Caddy:

```
# tavolfiore available at example.com/tavolfiore
# run with `./tavolfiore.rkt --root /tavolfiore`

example.com {
	rewrite /tavolfiore /tavolfiore/
	/tavolfiore/* {
		reverse_proxy http://localhost:58996
	}

	# other services...
}
```

OpenRC service files are provided under `openrc/`.

## Caveats

- Tavolfiore is meant to be used in a trusted group of people. There's no authentication; users identify themselves by just typing their name, without providing any password.

- Multiple simultaneous lists with a single instance of the server are not supported, so you can't plan more than one event at a time.

- At the moments there's no button to start planning a new session. You have to GET something like `example.com/tavolfiore/new-session?title=Awesome game night` (e.g. by entering it in your address bar) to do it.

## License

The code is licensed under the AGPL version 3 or later.
