#!/usr/bin/env racket

;; Copyright (C) 2022-2023  dalz
;;
;; This file is part of Tavolfiore.
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.
;;
;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

#lang racket/base

(require web-server/servlet
         web-server/servlet-env
         web-server/dispatch
         web-server/configuration/responders
         net/base64
         racket/cmdline
         racket/exn
         racket/file
         racket/runtime-path
         racket/set
         racket/string
         srfi/26)

(define-runtime-path servlets-root ".")
(define-runtime-path library-path "library")
(define-runtime-path planned-path "planned")
(define-runtime-path brought-last-time-path "brought-last-time")

(define port (make-parameter 58996))
(define root (make-parameter "/"))

(require "strings.rkt")
(define *s* (make-parameter #f))
(define (s label) ((*s*) label))

(define-syntax ~>
  (syntax-rules (<>)
    ((~> exp (fun <> args ...) rest ...)
     (~> (fun exp args ...) rest ...))
    ((~> exp (fun args ...) rest ...)
     (~> (fun args ... exp) rest ...))
    ((~> exp fun rest ...)
     (~> (fun exp) rest ...))
    ((~> exp) exp)))

(define (log-exn-return v)
  (lambda (e)
    (log-error (exn->string e))
    v))

(struct library-game
  (name time players)
  #:prefab)

(define library-games
  (with-handlers ((exn:fail? (log-exn-return (make-hash))))
    (with-input-from-file library-path (compose hash-copy read))))

(define (name->key n)
  (~> (string-normalize-spaces n #px"(\\s|\\p{P})+" "-")
      string-upcase))

(define (save-library!)
  (call-with-atomic-output-file
   library-path
   (lambda (port _) (write library-games port))))

(define (library-update! game #:replace (old-name #f))
  (let*  ((new-key (name->key (library-game-name game)))
          (old-key (and old-name (name->key old-name)))
          (key-change? (and old-name
                            (not (string=? (library-game-name game)
                                           old-name))))
          (new? (not (hash-has-key? library-games new-key))))
    (when key-change?
      (hash-remove! library-games old-key))
    (hash-set! library-games new-key game)
    (when (or new? key-change?)
      (library-invalidate-sort-cache!))))

(define (library-ref name-or-planned)
  (hash-ref
   library-games
   (if (string? name-or-planned)
     (name->key name-or-planned)
     (planned-game-key name-or-planned))))

(define-values (library-sorted-key/names! library-invalidate-sort-cache!)
  (let ((cache #f))
    (values
     (lambda ()
       (unless cache
         (set! cache
               (hash-map library-games
                         (lambda (key lg)
                           (cons key (library-game-name lg)))
                         #t)))
       cache)
     (lambda () (set! cache #f)))))

(struct reservation
  (player knows-rules?)
  #:mutable #:prefab)

(define (reservation-player? player resv)
  (string-ci=? (reservation-player resv) player))

(struct planned-game
  (key owner time-added resvs)
  #:mutable #:prefab)

(define id!
  (let ((n -1))
    (case-lambda
      (() (set! n (+ 1 n)) n)
      ((x) (set! n x)))))

(define-values (session-title planned-games)
  (let* ((exn-handler (log-exn-return (cons "Tavolfiore" (make-hasheqv))))
         (p (with-handlers ((exn:fail? exn-handler))
              (with-input-from-file planned-path read))))
    (values (car p)
            (hash-copy (cdr p)))))

(unless (hash-empty? planned-games)
  (id! (apply max (hash-keys planned-games))))

(define (save-planned!)
  (call-with-atomic-output-file
   planned-path
   (lambda (port _)
     (write (cons session-title planned-games) port))))

(define (planned-set! id pg)
  (hash-set! planned-games id pg)
  (planned-invalidate-sort-cache!))

(define (planned-ref id)
  (hash-ref planned-games id #f))

(define (planned-remove! id)
  (hash-remove! planned-games id)
  (planned-invalidate-sort-cache!))

(define-values (planned-sorted-id/games! planned-invalidate-sort-cache!)
  (let* ((cache-init '((name . #f) (resv . #f) (new . #f)))
         (cache (make-hash cache-init))
         (sort-by-name
          (lambda (a b)
            (string<? (planned-game-key a)
                      (planned-game-key b))))
         (sort-by-resv
          (lambda (a b)
            (> (length (planned-game-resvs a))
               (length (planned-game-resvs b)))))
         (sort-by-new
          (lambda (a b)
            (> (planned-game-time-added a)
               (planned-game-time-added b))))
         (sort-fn (lambda (by)
                    (let ((f (case by
                               ((name #f) sort-by-name)
                               ((resv) sort-by-resv)
                               ((new) sort-by-new))))
                      (lambda (a b) (f (cdr a) (cdr b)))))))
    (values
     (lambda (by)
       (or (hash-ref cache by #f)
           (let ((sorted (~> (hash->list planned-games)
                             (sort <> (sort-fn by)))))
             (hash-set! cache by sorted)
             sorted)))
     (lambda ((by #f))
       (if by
         (hash-set! cache by #f)
         (set! cache (make-hash cache-init)))))))

(define brought-last-time
  (with-handlers ((exn:fail? (log-exn-return (make-hash))))
    (with-input-from-file brought-last-time-path
      (lambda ()
        (hash-map/copy
         (read)
         (lambda (k l) (values k (list->set l)))
         #:kind 'mutable)))))

(define (new-session! title)
  (hash-clear! brought-last-time)

  (for ((pg (in-hash-values planned-games)))
    (let* ((owner (planned-game-owner pg))
           (brought (hash-ref! brought-last-time
                               (name->key owner)
                               (mutable-set))))
      (set-add! brought (planned-game-key pg))))

  (call-with-atomic-output-file
   brought-last-time-path
   (lambda (port _)
     (write (hash-map/copy
             brought-last-time
             (lambda (k s)
               (values (name->key k) (set->list s)))) port)))

  (hash-clear! planned-games)
  (planned-invalidate-sort-cache!)

  (set! session-title title)
  (save-planned!))

(define (render-body body (player #f)
                     #:splice? (splice? #f)
                     #:code (code 200) #:msg (msg #f) #:headers (hdrs '()))
  (response/xexpr
   #:headers hdrs
   #:preamble #"<!DOCTYPE html>\n"
   #:code code
   `(html ((lang ,(s 'lang)))
          (head
           (meta ((charset "UTF-8")))
           (meta ((name "viewport")
                  (content "width=device-width, initial-scale=1")))
           (base  ((href ,(root))))
           (title "Tavolfiore")
           (link ((rel "icon")
                  (href "favicon.png")
                  (type "image/png")))
           (link ((rel "stylesheet")
                  (href "stylesheet.css")
                  (type "text/css"))))

          (body
           ,@(if player
               `((form ((id "login-info") (action "logout"))
                       (label ,player) " "
                       (button ,(s 'logout))))
               '())

           ,@(if msg
               `((div ((class "error")) ,msg))
               '())
           ,@(if splice? body (list body))))))

(define (onclick-confirm label)
  `(onclick ,(format "return confirm(~s)"(s label))))

(define (planned->xexpr id pg player)
  (let* ((ids (number->string id))
         (lg (library-ref pg))
         (game-name (library-game-name lg))
         (owner (planned-game-owner pg))
         (ps (library-game-players lg))
         (min-ps (car ps))
         (max-ps (cdr ps))
         (resvs (planned-game-resvs pg))
         (player-resv (findf (cut reservation-player? player <>)
                             (planned-game-resvs pg)))
         (knows-rules-cb-id (string-append "knows-rules-" ids)))

    `(li ((class "game"))
         (h2 ((class "game-title")) ,game-name)

         (table
          (tr (th ,(s 'owner)) (td ,owner))
          (tr (th ,(s 'time)) (td ,(library-game-time lg)))
          (tr (th ,(s 'players))
              (td ,(string-append (number->string min-ps) "–"
                                  (number->string max-ps)))))

         (hr)
         (table ((class "reservations"))
                ,@(for/list (((r i) (in-indexed resvs)))
                    `(tr (td ,(if (< i max-ps) '() '((class "overbooked")))
                             ,(if (reservation-knows-rules? r) "★ " "")
                             ,(if (reservation-player? player r)
                                `(b ,player) (reservation-player r))))))

         ,@(if (null? resvs) '() '((hr)))

         (div ((class "game-buttons"))
              ,@(if (string-ci=? player owner)
                  `((form ((action "edit"))
                          (input ((name "old-name")
                                  (value ,game-name)
                                  (type "hidden")))
                          (button ((name "id") (value ,ids)) ,(s 'edit)))
                    (form ((action "delete") (method "post"))
                          (button ((name "id")
                                   (value ,ids)
                                   ,(onclick-confirm 'confirm-delete))
                                  ,(s 'delete)))
                    (br))
                  '())
              (form ((action "reserve") (method "post"))
                    (input ((type "checkbox")
                            (id ,knows-rules-cb-id)
                            (name "knows-rules")
                            ,@(if player-resv
                                (cons '(disabled "")
                                      (if (reservation-knows-rules? player-resv)
                                        '((checked "")) '()))
                                '())))
                    (label ((for ,knows-rules-cb-id)
                            (class ,(if player-resv "disabled" "")))
                           ,(s 'i-know-the-rules))
                    (br)
                    (button ((name "id")
                             (value ,ids))
                            ,(if player-resv
                               (s 'cancel-resv) (s 'reserve))))))))

(define (render-games player (sort-by 'name)
                      #:code (code 200) #:msg (msg #f) #:headers (hdrs '()))
  (define (sort-radios)
    (for/list ((by '(name resv new))
               (id (map (cut string-append "radio-" <>) '("name" "resv" "new")))
               (label (map s '(name reservations newly-added))))
      `(div ((style "display: inline"))
            (input ((id ,id)
                    (type "radio") (name "sort")
                    (value ,(symbol->string by))
                    ,@(if (eq? sort-by by) '((checked "")) '())))
            (label ((for ,id)) ,label))))

  (render-body
   `((script "setInterval(function(){window.location.reload()}, 1800000)")

     (h1 ,session-title)
     ,(s 'games-list-text)

     (form ((action "add")
            (style "display: inline"))
           (button ,(s 'bring)))
     (form ((action "delete-all")
            (method "post")
            (style "display: inline")
            ,(onclick-confirm 'confirm-delete-all))
           (button ,(s 'delete-all)))
     (br) (br)

     (form
      (button ,(s 'sort)) " " ,(s 'by)
      ,@(sort-radios))

     (ul ((id "games-container"))
         ,@(~> (planned-sorted-id/games! sort-by)
               (map (lambda (ig) (planned->xexpr (car ig) (cdr ig) player))))))

   player #:code code #:msg msg #:headers hdrs #:splice? #t))

(define (render-login #:continue (continue #f)
                      #:code (code 200) #:msg (msg #f))
  (render-body
   `((img ((src "tavolfiore.png") (alt "Tavolfiore") (id "logo")))
     ,@(s 'login-text)
     (form
      (input ((name "continue") (value ,(or continue (root))) (type "hidden")))
      (input ((name "player")
              (placeholder ,(s 'name))
              (autofocus ""))) " "
      (button ,(s 'login))))
   #:code code #:msg msg #:splice? #t))

(define (render-not-logged-in (continue #f))
  (render-login #:continue continue #:code 401 #:msg (s 'not-logged-in)))

(define (request-player req)
  (let* ((c (findf (lambda (c)
                     (string=? "tf-player" (client-cookie-name c)))
                   (request-cookies req)))
         (p (and c (~> c
                       client-cookie-value
                       string->bytes/utf-8
                       base64-decode
                       bytes->string/utf-8))))
    (and (non-empty-string? p) p)))

(define (handle-root req)
  (let* ((b (request-bindings req))
         (player (request-player req))
         (cookies (request-cookies req))
         (sort-by
          (if (exists-binding? 'sort b)
            (extract-binding/single 'sort b)
            (let ((c (findf (lambda (c)
                              (string=? "sort-by" (client-cookie-name c)))
                            cookies)))
              (and c (client-cookie-value c))))))

    (cond
      ((exists-binding? 'player b)
       (let* ((p (extract-binding/single 'player b))
              (c (make-cookie "tf-player"
                              (base64-encode (string->bytes/utf-8 p) #"")
                              #:max-age 31536000))
              (h (cookie->header c)))
         (redirect-to
          (if (exists-binding? 'continue b)
            (extract-binding/single 'continue b)
            (root))
          see-other #:headers (list h))))

      (player
       (let ((h (and (exists-binding? 'sort b)
                     (cookie->header
                      (make-cookie "sort-by" sort-by
                                   #:max-age 31536000)))))
         (render-games
          player (if sort-by (string->symbol sort-by) 'name)
          #:headers (if h (list h) '()))))

      (else (render-login)))))

(define (handle-logout _)
  (~> (make-cookie "tf-player" "")
      cookie->header
      list
      (redirect-to (root) see-other #:headers)))

(define (handle-new-session req)
  (let* ((b (request-bindings req))
         (title (extract-binding/single 'title b)))
    (new-session! title)
    (redirect-to (root) see-other)))

(define (handle-reserve req)
  (let* ((p (request-player req))
         (b (request-bindings req))
         (id (string->number (extract-binding/single 'id b)))
         (knows-rules? (exists-binding? 'knows-rules b))
         (pg (planned-ref id))
         (resvs (and pg (planned-game-resvs pg))))

    (cond
      ((not pg) (render-games p #:code 404 #:msg (s 'game-not-found)))
      ((not p) (render-not-logged-in))

      (else
       (set-planned-game-resvs!
        pg
        (if (member p resvs reservation-player?)
          (remove p resvs reservation-player?)
          (append resvs (list (reservation p knows-rules?)))))
       (planned-invalidate-sort-cache! 'resv)
       (save-planned!)
       (redirect-to (root) see-other)))))

(define (check-ownership f)
  (lambda (req)
    (let* ((p (request-player req)) ; TODO pass to f?
           (b (request-bindings req))
           (id (and (exists-binding? 'id b)
                    (string->number (extract-binding/single 'id b))))
           (pg (and id (planned-ref id)))
           (o (and pg (planned-game-owner pg))))
      (cond
        ((not p) (render-not-logged-in))
        ((not id) (f req))
        ((not pg) (render-games p #:code 404 #:msg (s 'game-not-found)))
        (else
         (if (string-ci=? p o)
           (f req)
           (render-games #:code 403 #:msg (s 'not-owner) p)))))))

(define (game-info-form (id "") (lg #f) (old-name ""))
  (let ((name (if lg (library-game-name lg) ""))
        (time (if lg (library-game-time lg) ""))
        (min-players (if lg (number->string (car (library-game-players lg))) ""))
        (max-players (if lg (number->string (cdr (library-game-players lg))) "")))

    `(form ((id "game-info-form") (action "edit") (method "post"))
           (input ((name "id") (value ,id) (type "hidden")))
           (input ((name "old-name") (value ,old-name) (type "hidden")))

           (table
            (tr (td (label ((for "name")) ,(s 'name)))
                (td (input ((name "name") (value ,name) (required "") (autofocus "")))))
            (tr (td (label ((for "time")) ,(s 'time)))
                (td (input ((name "time") (value ,time)))))
            (tr (td (label ((for "players")) ,(s 'players)))
                (td (input ((name "min-players")
                            (value ,min-players)
                            (type "number")
                            (required "")
                            (min "1")
                            (placeholder "2"))) " – "
                    (input ((name "max-players")
                            (value ,max-players)
                            (type "number")
                            (required "")
                            (min "1")
                            (placeholder "4")))))))))

(define (confirm/cancel-buttons (form-id #f))
  `(div
    (button ,(if form-id `((form ,form-id)) '()) ,(s 'confirm))
    (button ((form "cancel")) ,(s 'cancel))))

(define (cancel-form)
  `(form ((id "cancel") (action ,(root)) (style "display: none"))))

(define (toggle-visible id (form-id ""))
  (format
   "let s = document.getElementById('~a').style
    s.display = s.display == 'none' ? 'block' : 'none'
    if ('~a') {
      f = document.getElementById('~a')
      if (s.display == 'none')
        f.setAttribute('novalidate', '')
      else
        f.removeAttribute('novalidate')
    }"
   id form-id form-id))

(define (handle-add-get req)
  (let* ((p (request-player req))
         (pblt (hash-ref brought-last-time (name->key p) (mutable-set)))
         (game-info-form-body (cddr (game-info-form))))
    (if p
      (render-body
       `(,@(s 'bring-text)
         ,(cancel-form)
         (form ((id "form")
                (action "add")
                (method "post")
                (novalidate ""))
               ,(confirm/cancel-buttons)

               (select ((name "keys")
                        (size "10")
                        (multiple ""))
                       ,@(for/list ((kn (library-sorted-key/names!)))
                           `(option ((value ,(car kn))
                                     ,@(if (set-member? pblt (car kn))
                                         '((selected "")) '()))
                                    ,(cdr kn))))
               (div ((onclick
                      ,(string-append
                        (toggle-visible "game-info-form-div" "form")
                        "t = this.textContent
                         this.textContent =
                           (t[0] == '▸' ? '▾' : '▸') + t.substring(1)")))
                    "▸ " ,(s 'show-add-form))

               (div ((id "game-info-form-div")
                     (style "display: none"))
                    ,@game-info-form-body)))
       p #:splice? #t)
      (render-not-logged-in "add"))))

(define (handle-add-post req)
  (let* ((p (request-player req))
         (b (request-bindings req))
         (keys (extract-bindings 'keys b)))
    (cond
      ((not p) (render-not-logged-in "add"))
      ((and (non-empty-string? (extract-binding/single 'name b)))
       (handle-edit-post req))

      (else
       (for ((k keys))
         (planned-set! (id!) (planned-game k p (current-seconds) '())))
       (save-planned!)
       (redirect-to (root) see-other)))))

(define (handle-edit-get req)
  (let* ((p (request-player req))
         (b (request-bindings req))
         (id (extract-binding/single 'id b))
         (pg (and (non-empty-string? id)
                  (planned-ref (string->number id))))
         (lg (and pg (library-ref pg)))
         (old-name (extract-binding/single 'old-name b)))
    (render-body
     `(div
       ,(game-info-form id lg old-name)
       ,cancel-form
       ,(confirm/cancel-buttons "game-info-form"))
     p #:splice? #t)))

(define (handle-edit-post req)
  (let* ((p (request-player req))
         (b (request-bindings req))
         (id (extract-binding/single 'id b))
         (id (if (non-empty-string? id) (string->number id) (id!)))
         (new-name (extract-binding/single 'name b))
         (old-name (extract-binding/single 'old-name b))
         (time (extract-binding/single 'time b))
         (min-ps (string->number (extract-binding/single 'min-players b)))
         (max-ps (string->number (extract-binding/single 'max-players b)))
         (lg (library-game new-name time (cons min-ps max-ps)))
         (pg (planned-game (name->key new-name) p (current-seconds) '())))
    (if (and (non-empty-string? new-name)
             min-ps max-ps (< 0 min-ps (+ 1 max-ps)))
      (begin
        (planned-set! id pg)
        (save-planned!)
        (library-update! lg #:replace old-name)
        (save-library!)
        (redirect-to (root) see-other))
      (render-games p #:code 400 #:msg (s 'invalid-data)))))

(define (handle-delete-planned req)
  (define id (~> (request-bindings req)
                 (extract-binding/single 'id)
                 string->number))
  (planned-remove! id)
  (save-planned!)
  (redirect-to (root) see-other))

(define (handle-delete-all req)
  (define p (request-player req))
  (for (((id pg) (in-hash planned-games)))
    (when (string=? (name->key p)
                    (name->key (planned-game-owner pg)))
      (planned-remove! id)
      (save-planned!)))
  (redirect-to (root) see-other))

(define dispatcher
  (dispatch-case
   (("") handle-root)
   (("logout") handle-logout)
   (("new-session") handle-new-session)
   (("reserve") #:method "post" handle-reserve)
   (("add") handle-add-get)
   (("add") #:method "post" handle-add-post)
   (("edit") (check-ownership handle-edit-get))
   (("edit") #:method "post" (check-ownership handle-edit-post))
   (("delete") #:method "post" (check-ownership handle-delete-planned))
   (("delete-all") #:method "post" handle-delete-all)
   (("tavolfiore.png") (lambda (_) (file-response 200 #"" "tavolfiore.png")))
   (("favicon.png") (lambda (_) (file-response 200 #"" "favicon.png")))
   (("stylesheet.css")
    (let ((mime (header #"Content-Type" #"text/css")))
      (lambda (_) (file-response 200 #"" "stylesheet.css" mime))))))

(define (start req)
  (parameterize
      ((*s* (s-of (if (~> req request-headers
                          (assq 'accept-language)
                          (or <> '(() . "en")) cdr
                          (string-contains? <> "it"))
                    'it 'en))))
    (dispatcher req)))

(command-line
 #:program "Tavolfiore"
 #:once-each
 ("--port" port-number
           "Default: 58996"
           (port (string->number port-number)))
 ("--root" root-path
           "Default: /"
           (root (if (string-suffix? root-path "/")
                   root-path
                   (string-append root-path "/")))))

(serve/servlet
 start
 #:port (port)
 #:servlet-regexp #rx""
 #:stateless? #t
 #:command-line? #t
 #:servlets-root servlets-root)
